//! # RVM Opcodes
//!
//! `rvm_opcode` defines opcodes for the RVM virtual machine

//crate import statements
extern crate num;
#[macro_use]
extern crate num_derive;

//import
mod opcode;

//use statement
pub use opcode::*;

//end of file
